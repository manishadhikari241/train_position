import { Routes, Route } from "react-router-dom";
import { Index } from "../pages/index";
import Visualize from "../pages/visualize";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Index />} />
        <Route path="/visualize" element={<Visualize />} />
      </Routes>
    </>
  );
}

export default App;
