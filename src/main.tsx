import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
// import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { TrainsContextProvider } from "../context/TrainsContext";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <TrainsContextProvider>
        <App />
      </TrainsContextProvider>
    </BrowserRouter>
  </React.StrictMode>
);
