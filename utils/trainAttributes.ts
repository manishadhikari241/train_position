import { TrainLine, TrainLineColor } from "../enums/TrainLineColor"

export const convertSecondsToMin = (sec: number) => {
    return Math.floor(sec / 60)
}

export const getTrainLineColorMapping = () => {
    const trainLineColor = new Map();
    trainLineColor.set(TrainLine.BL, TrainLineColor.BLUE);
    trainLineColor.set(TrainLine.GR, TrainLineColor.BROWN);
    trainLineColor.set(TrainLine.OR, TrainLineColor.GREEN);
    trainLineColor.set(TrainLine.RD, TrainLineColor.ORANGE);
    trainLineColor.set(TrainLine.SV, TrainLineColor.PURPLE);
    trainLineColor.set(TrainLine.YL, TrainLineColor.RED);
    return trainLineColor;

}

export const getTrainLineColor = (lineCode: string | null) => {
    if (!lineCode) return 'white';
    return getTrainLineColorMapping().get(lineCode)

}