interface TrackCircuits {
    "seqNum": number,
    "CircuitId": number,
    "StationCode": string | null
}
export interface Routes {
    "LineCode": string,
    "TrackNum": number,
    "TrackCircuits": TrackCircuits[]
}
export interface StandardRoutes {
    "StandardRoutes": Routes[]

}