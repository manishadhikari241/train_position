export interface Trains {
    TrainId: string,
    TrainNumber: string,
    CarCount: number,
    DirectionNum: number,
    CircuitId: number,
    DestinationStationCode: null | string,
    LineCode: null | string,
    SecondsAtLocation: number,
    ServiceType: string
}
export interface TrainPositions {
    TrainPositions: Trains[]
}
