import React, { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTrains } from "../context/TrainsContext";
import { trainsApi } from "../services/trainApi";
import classes from "../styles/visualizeTrains.module.scss";
import { Routes } from "../types/standardRoutes";
import { getTrainLineColor } from "../utils/trainAttributes";

const VisualizeTrains: React.FC = () => {
  const { trainList } = useTrains();
  const navigate = useNavigate();

  const [standardRoutes, setStandardRoutes] = useState<Routes[]>([]);

  useEffect(() => {
    const getStandardRoutes = async () => {
      let routes = await trainsApi.getStrandardTrainRoutes();
      setStandardRoutes(routes.StandardRoutes);
    };
    getStandardRoutes();
  }, []);

  const getTrainsAsPerLine = useCallback((lineCode:string) => {
    return trainList.filter((trains) => trains.LineCode === lineCode);
  }, [trainList]);

  return (
    <>
      <button onClick={() => navigate("/")}>All Trains</button>

      <div>
        {standardRoutes.map((routes, index) => (
          <div key={index}>
            <div
              className={classes.header_item}
              style={{
                backgroundColor: getTrainLineColor(routes.LineCode),
              }}
            >
              <p>
                Train Line: {routes.LineCode}
                <br></br>
                Track No: {routes.TrackNum}
              </p>
            </div>
            <p>Trains on this Line</p>
            <div className={classes.header_sub_item}>
              {getTrainsAsPerLine(routes.LineCode).map((tracks, trackIndex) => (
                <div key={trackIndex} className={classes.box_train}>
                  {tracks.TrainNumber}
                </div>
              ))}
            </div>
            <p>Circuits</p>
            <div className={classes.header_sub_item}>
              {routes.TrackCircuits.map((tracks, trackIndex) => (
                <div key={trackIndex} className={classes.box}>
                  {tracks.CircuitId}
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default VisualizeTrains;
