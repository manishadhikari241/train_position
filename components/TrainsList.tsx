import classes from "../styles/trainList.module.scss";
import { useNavigate } from "react-router-dom";

import {
  convertSecondsToMin,
  getTrainLineColor,
} from "../utils/trainAttributes";
import { TrainLine, TrainLineColor } from "../enums/TrainLineColor";
import { useTrains } from "../context/TrainsContext";
import { useEffect, useState } from "react";
import { Trains } from "../types/TrainLists";
import React from "react";
import { TrainService } from "../enums/TrainService";

const TrainsList = () => {
  const { trainList } = useTrains();
  const navigate = useNavigate();
  const [selectedTrainColorFilter, setSelectedTrainColorFilter] = useState("");
  const [selectedTrainServiceFilter, setSelectedTrainServiceFilter] =
    useState("");

  const [filteredTrainList, setFilteredTrainList] = useState<Trains[]>([]);

  useEffect(() => {
    setFilteredTrainList(trainList);
  }, [trainList]);

  const filterByTrainColor = (lineCode: string) => {
    setSelectedTrainColorFilter(lineCode);

    const filteredTrains = filteredTrainList.filter(
      (trains) => trains.LineCode === lineCode
    );
    setFilteredTrainList(filteredTrains);
  };
  const filterByTrainService = (service: string) => {
    setSelectedTrainServiceFilter(service);

    const filteredTrains = filteredTrainList.filter(
      (trains) => trains.ServiceType === service
    );
    setFilteredTrainList(filteredTrains);
  };

  const resetFilter = () => {
    setFilteredTrainList(trainList);
    setSelectedTrainColorFilter("");
    setSelectedTrainServiceFilter("");

  };
  return (
    <>
      <button onClick={() => navigate("/visualize")}>Visualize</button>
      <button className={classes.reset_btn} onClick={resetFilter}>
        Reset Filter
      </button>

      <div className={classes.filter}>
        <div className={classes.filter_item}>
          <label>Filter By Train Line color:</label>
          <select
            value={selectedTrainColorFilter}
            onChange={(e) => filterByTrainColor(e.target.value)}
          >
            <option value="" disabled selected>
              -- select an option --
            </option>
            <option value={TrainLine.BL}>
              {TrainLineColor.BLUE} - {TrainLine.BL}{" "}
            </option>
            <option value={TrainLine.YL}>{TrainLineColor.RED}</option>
            <option value={TrainLine.OR}>{TrainLineColor.GREEN}</option>
            <option value={TrainLine.SV}>{TrainLineColor.PURPLE}</option>
            <option value={TrainLine.RD}>{TrainLineColor.ORANGE}</option>
            <option value={TrainLine.GR}>{TrainLineColor.BROWN}</option>
          </select>
        </div>
        <div className={classes.filter_item}>
          <label>Filter By Service:</label>
          <select
            value={selectedTrainServiceFilter}
            onChange={(e) => filterByTrainService(e.target.value)}
          >
            <option value="" disabled selected>
              -- select an option --
            </option>
            <option value={TrainService.NORMAL}>{TrainService.NORMAL}</option>
            <option value={TrainService.NO_PASSENGERS}>
              {TrainService.NO_PASSENGERS}
            </option>
            <option value={TrainService.SPECIAL}>{TrainService.SPECIAL}</option>
            <option value={TrainService.UNKNOWN}>{TrainService.UNKNOWN}</option>
          </select>
        </div>
      </div>
      <div className={classes.header}>
        <div className="left_item">Trains</div>
        <div className="right_item">Minues At Location</div>
      </div>
      <div className={classes.items}>
        {filteredTrainList.map((trains, index) => (
          <div
            key={index}
            style={{ backgroundColor: getTrainLineColor(trains.LineCode) }}
            className={classes.item_box}
          >
            <div className="left_info">
              <p>
                Train no: {trains.TrainNumber}
                <br></br>
                Cars: {trains.CarCount}
              </p>
            </div>
            <div className="left_info">
              <p>
                Train Line: {trains.LineCode}
                <br></br>
                Service Type: {trains.ServiceType}
                <br></br>
                Circuit Number: {trains.CircuitId}
              </p>
            </div>
            <div className="right_info">
              <p>{convertSecondsToMin(trains.SecondsAtLocation)} min</p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default TrainsList;
