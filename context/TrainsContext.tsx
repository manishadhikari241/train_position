import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { trainsApi } from "../services/trainApi";
import { Trains } from "../types/TrainLists";

interface TrainsContextProviderProps {
  children: ReactNode;
}
interface TrainsContextProps {
  trainList: Trains[];
}

const TrainsContext = createContext<TrainsContextProps>({
  trainList: [],
});

export const useTrains = () => {
  return useContext(TrainsContext);
};

export const TrainsContextProvider = ({
  children,
}: TrainsContextProviderProps) => {
  const [trainList, setTrainList] = useState<Trains[]>([]);

  useEffect(() => {
    const getTrainsInterval = async () => {
      let trainsData = await trainsApi.getTrains();
      setTrainList(trainsData.TrainPositions);
    };
    getTrainsInterval().catch((error) => console.log(error));
    // const interval = setInterval(() => getTrainsInterval(), 10000);
    // return () => {
    //   clearInterval(interval);
    // };
  }, []);

  return (
    <TrainsContext.Provider value={{ trainList }}>
      {children}
    </TrainsContext.Provider>
  );
};
