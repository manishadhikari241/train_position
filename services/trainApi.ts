import { StandardRoutes } from '../types/standardRoutes';
import { TrainPositions } from '../types/TrainLists'
const API_KEY: string = "f6395b50097040038eb91ceca8eb2470";

const getTrains = async (): Promise<TrainPositions> => {
    let response = await fetch(`/TrainPositions/TrainPositions?contentType=json`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api_key': API_KEY
        }
    })
    return response.json()
}
const getStrandardTrainRoutes = async (): Promise<StandardRoutes> => {
    let response = await fetch(`/TrainPositions/standardRoutes?contentType=json`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api_key': API_KEY
        }
    })
    return response.json()
}

export const trainsApi = {
    getTrains,
    getStrandardTrainRoutes
}