import React, { ReactNode } from "react";
import classes from "../styles/homeLayout.module.scss";

interface HomeLayoutProps {
  children: ReactNode;
}

const HomeLayout = ({ children }: HomeLayoutProps) => {
  return <div className={classes.main_wrapper}>{children}</div>
};

export default HomeLayout;
