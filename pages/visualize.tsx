import React from "react";
import VisualizeTrains from "../components/VisualizeTrains";
import HomeLayout from "../layouts/HomeLayout";

const Visualize = () => {
  return (
    <HomeLayout>
      <VisualizeTrains></VisualizeTrains>
    </HomeLayout>
  );
};

export default Visualize;
