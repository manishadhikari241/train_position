import React from "react";
import TrainsList from "../components/TrainsList";
import HomeLayout from "../layouts/HomeLayout";

export const Index = () => {
  return (
    <HomeLayout>
      <TrainsList></TrainsList>;
    </HomeLayout>
  );
};
