export enum TrainService {
    NO_PASSENGERS = 'NoPassengers',
    NORMAL = 'Normal',
    SPECIAL = 'Special',
    UNKNOWN = 'Unknown'

}