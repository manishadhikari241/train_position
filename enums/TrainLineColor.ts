export enum TrainLine {
    BL = 'BL',
    GR = 'GR',
    OR = 'OR',
    RD = 'RD',
    SV = 'SV',
    YL = 'YL',

}
export enum TrainLineColor {
    GREEN = 'green',
    RED = 'red',
    BLUE = 'blue',
    PURPLE = 'purple',
    BROWN = 'brown',
    ORANGE = 'orange',

}